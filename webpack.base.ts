import { Configuration } from "webpack";
import { TsConfigPathsPlugin } from "awesome-typescript-loader";
import lodash from "lodash";
import * as path from "path";
import { DIST, WEBPACK } from "./src/shared/config";
import MiniCSSExtractPlugin from "mini-css-extract-plugin";


export let mode: Configuration["mode"];

switch (process.env.NODE_ENV) {
    case "development":
    case "production":
        mode = process.env.NODE_ENV;
        break;
    case "none":
    default:
        mode = "production";
        break;
}

console.log(`MODE: ${ mode }`);

export const baseConfig: Configuration = {
    mode,
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                loader: "awesome-typescript-loader",
            },
        ],
    },
    resolve: {
        extensions: [ ".js", ".ts", ".jsx", ".tsx" ],
        plugins: [
            new TsConfigPathsPlugin(),
        ],
    },
};

if (mode === "development") {
    baseConfig.devtool = "source-map";
}

export function mergeWith(...obj: Configuration[]): Configuration {
    return lodash.mergeWith({}, baseConfig, ...obj, function (a: any, b: any) {
        if (lodash.isArray(a)) {
            return a.concat(b);
        }
    });
}
