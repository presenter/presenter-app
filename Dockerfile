FROM node:alpine as builder

ARG NODE_ENV=production

WORKDIR /build

COPY package.json .
COPY package-lock.json .

RUN npm ci

COPY webpack webpack
COPY src src
COPY assets assets

COPY tsconfig.json .
COPY webpack.base.ts .
COPY webpack.config.ts .

RUN npm run build

FROM node:alpine as app

ARG NODE_ENV=production
ENV NODE_ENV $NODE_ENV

WORKDIR /app

COPY --from=builder /build/dist/manifest.json .
COPY --from=builder /build/dist/server.js .

ENTRYPOINT node $NODE_OPTS server.js

FROM halverneus/static-file-server as static

WORKDIR /static
ENV FOLDER /static

COPY --from=builder /build/dist/www .
COPY --from=builder /build/dist/assets assets
