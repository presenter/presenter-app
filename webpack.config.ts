import { clientConfig } from "./src/client/webpack.config";
import { serverConfig } from "./src/server/webpack.config";

// noinspection JSUnusedGlobalSymbols
export default [ serverConfig, clientConfig ];
