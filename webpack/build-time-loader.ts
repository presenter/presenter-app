import { RawSourceMap } from "source-map";
import * as webpack from "webpack";
import LoaderContext = webpack.loader.LoaderContext;

export default function (this: LoaderContext, content: string, map: RawSourceMap | undefined, meta: any) {
    const callback = this.async();

    if (!callback) {
        throw new Error("callback is undefined");
    }

    import(this.resourcePath)
        .then(module => `module.exports = ${ JSON.stringify(module) }`)
        // @ts-ignore
        .then(result => callback(null, result));
}
