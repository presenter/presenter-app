#!/usr/bin/env bash

cd "$(dirname "$0")" || exit

docker build -t presenter-app --target app --build-arg "NODE_ENV=$NODE_ENV" .
docker build -t presenter-app-static --target static --build-arg "NODE_ENV=$NODE_ENV" .

for POD in $(kubectl get pod --selector app=presenter-app -o name -n presenter) ; do
  kubectl delete "$POD" -n presenter
done
