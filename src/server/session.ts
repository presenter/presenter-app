import session from "express-session";


const secret = process.env.SESSION_SECRET;
export const memoryStore = new session.MemoryStore();

if (!secret) {
    throw new Error("SESSION_SECRET not defined");
}

export const middleware = () => session({
    secret,
    store: memoryStore,
    resave: false,
    saveUninitialized: false
});

export { middleware as session };
