import React from "react";
import * as fs from "fs";
import * as path from "path";
import { ServerStyleSheets } from "@material-ui/core/styles";
import { renderToNodeStream, renderToStaticMarkup } from "react-dom/server";
import { JWTProvider } from "@shared/app/jwt";
import { createStore } from "redux";
import { reducers } from "@shared/store/reducer";
import { enhancers } from "@shared/store";
import { createSSRContext, SSRContext } from "@shared/ssr";
import { StaticRouter } from "react-router-dom";
import { Provider as Store } from "react-redux";
import { ComponentRoute, ServerRoute } from "@routing/models/route";
import { Application } from "@shared/app/application";


const manifest = JSON.parse(fs.readFileSync(path.join(__dirname, "manifest.json"), "utf8"));

interface RenderOptions {
    route: ComponentRoute,
    csrf: JSX.Element,
    jwt?: string
}

function resource(path: string) {
    return process.env.STATIC_HOST ? new URL(path, process.env.STATIC_HOST).toString() : path;
}

export function renderHTML(options: RenderOptions) {
    const fonts = "https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap";
    const sheets = new ServerStyleSheets();
    const store = createStore(reducers, enhancers());
    const ssr = createSSRContext();

    const { route } = options;

    const app = () => (
        <StaticRouter location={ route.path }>
            <SSRContext value={ ssr }>
                <Store store={ store }>
                    <JWTProvider token={ options.jwt }>
                        <Application/>
                    </JWTProvider>
                </Store>
            </SSRContext>
        </StaticRouter>
    );

    renderToStaticMarkup(sheets.collect(app()));

    return renderToNodeStream(
        <html lang="en">
        <head>
            <title>{ store.getState().application.title }</title>
            <link rel="stylesheet" href={ resource(manifest["client.css"]) }/>
            <link rel="stylesheet" href={ fonts }/>
            <meta name="viewport" content="width=device-width, initial-scale=1"/>
            { route.description && <meta name="description" content={ route.description }/> }
            { options.csrf }
            <script src={ resource(manifest["runtime.js"]) } defer/>
            <script src={ resource(manifest["vendors~client.js"]) } defer/>
            <script src={ resource(manifest["client.js"]) } defer/>
            <style id="ssr-css" dangerouslySetInnerHTML={ { __html: sheets.toString() } }/>
        </head>
        <body>
        <div id="root">
            { app() }
        </div>
        <script
            dangerouslySetInnerHTML={ { __html: `window.__PRELOADED_STATE__ = ${ JSON.stringify(store.getState()) }` } }
        />
        </body>
        </html>
    );
}
