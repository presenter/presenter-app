import Keycloak, { Grant } from "keycloak-connect";
import Token from "keycloak-connect/middleware/auth-utils/token";
import { memoryStore } from "./session";
import { NextFunction, Request, RequestHandler, Response } from "express";
import { Cookies } from "./cookies";


declare global {
    namespace Express {
        export interface Request {
            kauth?: {
                grant?: Grant
            }
        }
    }
}

declare module "keycloak-connect" {
    export interface Token {
        token: string
        header: { [k: string]: any }
        content: { [k: string]: any }
        signature: Buffer
    }
}

if (!process.env.KEYCLOAK_URL) {
    throw new Error("KEYCLOAK_URL not defined");
}


const keycloak = new Keycloak({
    store: memoryStore,
}, {
    "realm": "presenter",
    "auth-server-url": process.env.KEYCLOAK_URL,
    "ssl-required": "external",
    "resource": "presenter-app",
    // "bearer-only": true,
    // @ts-ignore
    "public-client": true,
    "confidential-port": 0,
});

function removeCookies(req: Request, omit?: Cookies[]) {
    const { res } = req;

    if (res) {
        for (const name of [
            Cookies.payload,
            Cookies.signature,
            Cookies.connectSID,
            Cookies.refreshToken,
        ]) {
            if (name in req.cookies && !omit?.includes(name)) {
                res.cookie(name, null, { maxAge: 0 });
            }
        }
    }
}

function getTokenMaxAge(token: Token) {
    const exp = token.content.exp * 1000;
    return exp - Date.now();
}

keycloak.authenticated = function (req) {
    const { res } = req;
    const grant = req.kauth?.grant;

    if (res && grant) {
        if (grant.access_token) {
            const [ header, content, sig ] = grant.access_token.token.split(".");

            const maxAge = getTokenMaxAge(grant.access_token);

            res.cookie(Cookies.payload, [ header, content ].join("."), { maxAge });
            res.cookie(Cookies.signature, sig, { httpOnly: true, maxAge });
        }

        if (grant.refresh_token) {
            res.cookie(Cookies.refreshToken, grant.refresh_token.token, {
                httpOnly: true,
                maxAge: getTokenMaxAge(grant.refresh_token),
            });
        }
    }
};

keycloak.deauthenticated = removeCookies;

export function checkJWT(): RequestHandler {
    return function (req, res, next) {
        if (req.cookies[Cookies.payload] !== req.kauth?.grant) {
            createGrantFromJWT(req, res, next);
        } else {
            if (!req.cookies[Cookies.payload] || !req.cookies[Cookies.signature]) {
                keycloak.authenticated(req);
            }

            next();
        }
    };
}

export function createGrantFromJWT(req: Request, res: Response, next: NextFunction) {
    if (req.kauth?.grant) {
        return next();
    }

    const { payload, signature, refresh_token } = req.cookies;

    if (payload && signature) {
        const clientID = (keycloak.getConfig() as any).clientId;
        const data = [ payload, signature ].join(".");
        const token = new Token(data, clientID);

        if (token.isExpired()) {
            return next();
        }
        keycloak.grantManager.createGrant({
            access_token: token,
            refresh_token: refresh_token ? new Token(refresh_token, clientID) : undefined,
        })
            .then(grant => {
                if (grant) {
                    keycloak.storeGrant(grant, req, res);
                }
            })
            .catch(err => {
                console.error(err);
                removeCookies(req);
            })
            .finally(() => next());
    } else {
        removeCookies(req, [ Cookies.connectSID ]);
        next();
    }
}

export { keycloak } ;
