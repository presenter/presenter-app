declare module "keycloak-connect/middleware/auth-utils/token" {
    export { Token } from "keycloak-connect";

    type Token = Token;

    const Token: Token & { new(token: string, clientID: string): Token };

    export default Token;
}

declare module "keycloak-connect/uuid" {
    const UUID: () => string;
    export default UUID;
}
