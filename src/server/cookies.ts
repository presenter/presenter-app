export enum Cookies {
    connectSID   = "connect.sid",
    payload      = "payload",
    signature    = "signature",
    refreshToken = "refresh-token",
}
