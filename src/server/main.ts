import express from "express";
import * as path from "path";
import morgan from "morgan";
import * as fs from "fs";
import cookieParser from "cookie-parser";
import { keycloak } from "./keycloak";
import { session } from "./session";
import UUID from "keycloak-connect/uuid";
import { DIST } from "@shared/config";
import { loadRoutes } from "./route-loader";
import { routes } from "@shared/app/application";


const app = express();

app.use(morgan("tiny"));

app.use(cookieParser());
app.use(session());
app.use(keycloak.middleware());

if (!process.env.STATIC_HOST) {
    app.use(express.static(path.join(DIST, "/assets")));
    app.use(express.static(path.join(DIST, "/www")));
}

app.get("/favicon.ico", function (req, res) {
    const favicon = path.join(__dirname, "favicon.ico");

    if (fs.existsSync(favicon)) {
        res.sendFile(favicon);
    } else {
        res.status(404);
        res.end();
    }
});

app.get("/account", function (req, res) {
    res.redirect(new URL("/auth/realms/presenter/account/", process.env.KEYCLOAK_URL).toString());
});

app.get("/login", function (req, res) {
    if (!keycloak.redirectToLogin(req)) {
        res.redirect("/");
    } else {
        let redirect: string | undefined;

        const referer = req.header("referer");
        const host = req.get("host");

        if (referer) {
            const url = new URL(referer);

            if (url.host === host) {
                redirect = url.protocol + "//" + url.host + url.pathname;
            }
        }

        if (!redirect) {
            redirect = req.protocol + "://" + host + "/";
        }

        const sep = redirect.includes("?") ? "&" : "?";

        redirect += sep + "auth_callback=1";

        console.log(redirect);

        if (req.session) {
            req.session.auth_redirect_uri = redirect;
        }

        res.redirect(keycloak.loginUrl(UUID(), redirect));
    }

    res.end();
});


app.use(keycloak.checkSso());

app.use(loadRoutes(routes))

const server = app.listen(process.env.PORT || 8080, function () {
    let address: any = server.address();

    if (address && "port" in address) {
        address = address.port;
    }

    console.log("Server started on", address);
});
