import { mergeWith } from "../../webpack.base";
import path from "path";
import CopyWebpackPlugin from "copy-webpack-plugin";
import { DIST, ROOT } from "../shared/config";

export const serverConfig = mergeWith({
    entry: { server: path.join(__dirname, "main.ts") },
    node: {
        __dirname: false,
    },
    output: {
        path: DIST,
    },
    module: {
        rules: [
            {
                test: /\.scss$/,
                loader: "null-loader",
            }
        ],
    },
    target: "node",
    plugins: [
        new CopyWebpackPlugin({
            patterns: [ {
                from: path.join(ROOT, "assets"),
                to: path.join(ROOT, "dist/assets"),
            } ],
        })
    ],
});
