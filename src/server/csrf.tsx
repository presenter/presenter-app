import cryptoRandomString from "crypto-random-string";
import { Response } from "express";
import React from "react";


export function generateCSRF(res: Response) {
    const value = cryptoRandomString({ length: 64, type: "url-safe" });

    res.cookie("csrf", value, { httpOnly: true });

    return (
        <meta name="csrf" content={ value }/>
    );
}
