import { ComponentRoute, ServerRoute } from "@routing/models/route";
import { checkJWT, keycloak } from "./keycloak";
import { generateCSRF } from "./csrf";
import express, { RequestHandler, Router } from "express";
import { renderToStaticNodeStream } from "react-dom/server";
import React from "react";
import { renderHTML } from "./webpage";
import { isModuleRoute } from "@routing/index";

function render({ res, ...forward }: {
  res: express.Response,
  csrf: JSX.Element,
  route: ComponentRoute,
  jwt?: string
}) {
  const stream = renderHTML(forward);
  stream.pipe(res);
}

function serveRoute(route: ComponentRoute): RequestHandler {
  return (req, res, next) => {
    res.status(200);
    res.header("Content-Type", "text/html");

    const csrf = generateCSRF(res);

    res.write("<!DOCTYPE html>");

    const config = {
      res,
      csrf,
      route: { ...route, path: req.originalUrl },
    };

    keycloak.getGrant(req, res)
        .then(grant => {
          render(Object.assign(
              config,
              {
                jwt: grant.access_token?.token,
              },
          ));
        })
        .catch(err => {
          render(config);
        });
  };
}

export function loadRoutes(routes: ServerRoute[]): Router {
  const router = Router();

  for (const route of routes) {
    if (isModuleRoute(route)) {
      router.use(route.path, loadRoutes(route.module.routes));
    } else {
      router[route.exact ? "get" : "use"](route.path, checkJWT(), serveRoute(route));
    }
  }

  return router;
}
