import { ofType } from "@shared/store/effects";
import { applicationActions } from "@shared/app/store/actions";
import { map } from "rxjs/operators";
import { Subject } from "rxjs";
import { AnyAction } from "redux";

const clientEffect$ = new Subject<AnyAction>();

clientEffect$.pipe(
    ofType(applicationActions.updateTitle),
    map(action => action.title),
).subscribe(value => document.title = value)

export default clientEffect$;
