import { mergeWith } from "../../webpack.base";
import path from "path";
import { DIST, WEBPACK } from "../shared/config";
import MiniCSSExtractPlugin from "mini-css-extract-plugin";
import WebpackManifestPlugin from "webpack-manifest-plugin";
import { CleanWebpackPlugin } from "clean-webpack-plugin";

export const clientConfig = mergeWith({
    entry: { client: path.join(__dirname, "main.tsx") },
    output: {
        filename: "[name].js",
        path: path.join(DIST, "www"),
    },
    module: {
        rules: [
            // {
            //     test: /\.scss$/,
            //     use: [
            //         {
            //             loader: MiniCSSExtractPlugin.loader,
            //         },
            //         {
            //             loader: "css-loader",
            //             options: {
            //                 importLoaders: 1,
            //             },
            //         },
            //         "sass-loader",
            //     ],
            // },
            {
                test: /\.scss$/,
                use: [
                    MiniCSSExtractPlugin.loader,
                    "css-loader",
                    "sass-loader",
                ],
            },
        ],
    },
    plugins: [
        new CleanWebpackPlugin(),
        new WebpackManifestPlugin({
            publicPath: "/",
            fileName: "../manifest.json",
        }),
        new MiniCSSExtractPlugin({
            chunkFilename: "styles.css",
        }),
    ],
    optimization: {
        splitChunks: {
            chunks: "all",
        },
        runtimeChunk: "single",
    },
    target: "web",
});
