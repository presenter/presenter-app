import React, { PropsWithChildren, ReactNode } from "react";
import ReactDOM from "react-dom";
import { JWTProvider } from "@shared/app/jwt";
import { Cookies } from "../server/cookies";
import { createStore } from "redux";
import { reducers } from "@shared/store/reducer";
import { effect$, enhancers } from "@shared/store";
import clientEffect$ from "./effects";
import { CookiesProvider, useCookie } from "@shared/app/cookies";
import { Provider as Store } from "react-redux";
import { BrowserRouter } from "react-router-dom";
import { Application } from "@shared/app/application";

const root = document.getElementById("root");


function JWT({ children }: PropsWithChildren<{}>) {
    const token = useCookie(map => map[Cookies.payload]);

    return (
        <JWTProvider token={ token }>
            { children }
        </JWTProvider>
    );
}

function provide(children: ReactNode) {
    const initialState = window.__PRELOADED_STATE__;
    delete window.__PRELOADED_STATE__;

    effect$.subscribe(clientEffect$);

    return (
        <BrowserRouter>
            <Store store={ createStore(reducers, initialState, enhancers()) }>
                <CookiesProvider>
                    <JWT>
                        { children }
                    </JWT>
                </CookiesProvider>
            </Store>
        </BrowserRouter>
    );
}

ReactDOM.hydrate(provide(<Application/>), root);
