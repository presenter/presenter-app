type Callback<Args extends any[]> = (...args: Args) => any;

export function invokeAll<Args extends any[]>(...functions: (Callback<Args> | ((...args: any[]) => any) | undefined | null)[]) {
    return (...args: Args) => functions
        .filter((fn): fn is Callback<Args> => !!fn)
        .map(fn => fn(...args));
}
