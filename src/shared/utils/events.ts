export function pluckTargetValue<T>(event: { target: { value: T } }) {
    return event.target.value;
}

