function key<T>(value: T, fn: keyof T | ((value: T) => any)) {
  return typeof fn === "function" ? fn(value) : value[fn];
}

export function createMap<T>(arr: T[], fn: (value: T) => string): { [k: string]: T };
export function createMap<T>(arr: T[], fn: (value: T) => number): { [k: number]: T };
export function createMap<T>(arr: T[], property: keyof T): { [k: string]: T };
export function createMap<T>(arr: T[], fn: keyof T | ((value: T) => any)) {
  return arr.reduce((map, val) => Object.assign(map, { [key(val, fn)]: val }), {});
}
