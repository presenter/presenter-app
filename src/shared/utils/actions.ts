import { Action } from "redux";

export type ActionCreator<Type extends string = string, Props = any, Args extends any[] = any> =
    { type: Type }
    & ((...args: Args) => Action<Type> & Props);

export function makeActionCreator<Type extends string>(type: Type): ActionCreator<Type, {}, []>;
export function makeActionCreator<Type extends string, Args extends any[], Props>(type: Type, fn: (...args: Args) => Props): ActionCreator<Type, Props, Args>;
export function makeActionCreator<Type extends string>(type: Type, fn?: Function): () => any {
    const action = { type };

    return Object.assign(
        (...args: any[]) => fn ? Object.assign(action, fn(...args)) : action,
        { type },
    );
}

export type ActionCollection = { [k: string]: ActionCreator<string, any, any> };

export type ActionsOf<T extends ActionCollection> = ReturnType<T[keyof T]>
