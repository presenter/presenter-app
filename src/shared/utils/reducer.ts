import { ActionCreator } from "./actions";
import { Action } from "redux";

type ActionHandler<S, A extends ActionCreator> = (state: S, actions: ReturnType<A>) => S;

type Reducer<S, A extends Action> = (state: S | undefined, action: A) => S;

function makeReducer<State>(initialState: State, ...cases: { type: string, fn: ActionHandler<State, ActionCreator> }[]): Reducer<State, ActionCreator> {
    const map = cases.reduce((map: any, handler) => Object.assign(
        map, { [handler.type]: [ ...(map[handler.type] || []), handler.fn ] },
    ), {});

    return function (state = initialState, action): State {
        return map[action.type].reduce((state: State, fn: ActionHandler<State, typeof action>) => fn(state, action), state);
    };
}


function on<S, A extends ActionCreator>(actionCreator: A, fn: ActionHandler<S, A>): { type: A["type"], fn: ActionHandler<S, A> } {
    return { type: actionCreator.type, fn };
}

