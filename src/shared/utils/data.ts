import React, { useCallback } from "react";

export class Reference<O, K extends keyof O> {
    constructor(public readonly  object: O, public readonly key: K) {}

    set value(value: O[K]) { this.object[this.key] = value; }

    get value() { return this.object[this.key]; }
}

type Target<> = Reference<any, any> | React.Dispatch<any> ;

function deps(value: Target) {
    if (value instanceof Reference) {
        return [ value.object, value.key ];
    } else if (typeof value === "function") {
        return [ value ];
    }

    throw new Error("invalid value");
}

function update(dispatch: Target, value: any) {
    if (dispatch instanceof Reference) {
        dispatch.value = value;
    } else if (typeof dispatch === "function") {
        dispatch(value);
    }
}

export function useToggle<O, K extends keyof O, Args extends any[]>(reference: Reference<any, any>) {
    return useCallback(
        () => reference.value = !reference.value,
        [ ...deps(reference) ],
    );
}
