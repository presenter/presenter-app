import { Action, compose } from "redux";
import { useCallback } from "react";
import { useDispatch } from "react-redux";

export function useDispatchCallback<A, Args extends any[], R extends Action>(
    f1: (a: A) => R,
    f2: (...args: Args) => A,
): (...args: Args) => void;
export function useDispatchCallback<A, B, Args extends any[], R extends Action>(
    f1: (a: B) => R,
    f2: (a: A) => B,
    f3: (...args: Args) => A,
): (...args: Args) => void;
export function useDispatchCallback<A, B, C, Args extends any[], R extends Action>(
    f1: (b: C) => R,
    f2: (a: B) => C,
    f3: (a: A) => B,
    f4: (...args: Args) => A,
): (...args: Args) => void;

export function useDispatchCallback<A, Args extends any[], R extends Action>(
    deps: any[],
    f1: (a: A) => R,
    f2: (...args: Args) => A,
): (...args: Args) => void;
export function useDispatchCallback<A, B, Args extends any[], R extends Action>(
    deps: any[],
    f1: (a: B) => R,
    f2: (a: A) => B,
    f3: (...args: Args) => A,
): (...args: Args) => void;
export function useDispatchCallback<A, B, C, Args extends any[], R extends Action>(
    deps: any[],
    f1: (b: C) => R,
    f2: (a: B) => C,
    f3: (a: A) => B,
    f4: (...args: Args) => A,
): (...args: Args) => void;

export function useDispatchCallback(...args: any[]) {
    let [ deps ] = args;
    let operators;

    if (typeof deps === "function") {
        deps = [];
        operators = args;
    } else {
        operators = args.slice(1);
    }

    const dispatch = useDispatch();

    return useCallback(
        compose(
            dispatch,
            ...operators,
        ),
        deps
    );
}
