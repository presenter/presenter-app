import * as path from "path";

export const ROOT    = path.resolve("."),
             WEBPACK = path.join(ROOT, "webpack"),
             DIST    = path.join(ROOT, "dist");
