import { createContext, createElement, PropsWithChildren, useContext, useEffect } from "react";

interface Effect {
    callback: () => void
    deps: any[]
}

class Context {
    constructor(public readonly effects: Effect[]) {}

    process(cb: () => void, deps: any[]) {
        const effect = this.effects[this.index];

        if (!effect || effect.deps.some((dep, i) => deps[i] !== dep)) {
            cb();

            this.effects[this.index] = {
                callback: cb,
                deps,
            };
        }
    }

    index = 0;
}

const context = createContext<Context | undefined>(undefined);

export function createSSRContext() {
    return new Context([]);
}

export function SSRContext({ value, children }: PropsWithChildren<{ value: Context }>) {
    return createElement(context.Provider, {
        value: new Context(value.effects),
    }, children);
}

function useSSRContext(): Context {
    return useContext(context) as Context;
}

export function useSSREffect(cb: () => void, deps: any[]) {
    if (typeof window === "undefined") {
        const ctx = useSSRContext();
        ctx.process(cb, deps);
    } else {
        useEffect(cb, deps);
    }
}
