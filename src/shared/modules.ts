declare module "isomorphic-style-loader/StyleContext" {
    interface StyleContextValue {
        insertCss?: Function
    }

    const context: import("react").Context<StyleContextValue>;
    export default context;
}
declare module "isomorphic-style-loader/useStyles" {
    const s: any;
    export default s;
}

declare module "*.scss" {
    const style: any;
    export default style;
}
