import { Thunder } from "@graphql/graphql-zeus";
import { useHTTP } from "../network/http";
import { thunderConfig } from "./config";

export function useThunder<K extends keyof typeof thunderConfig["api"]>(api: K) {
    const http = useHTTP();

    return Thunder(query => http
        .post(thunderConfig.api[api], JSON.stringify({ query }))
        .then(json => json?.data),
    );
}
