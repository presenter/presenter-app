import React, { createContext, PropsWithChildren, useContext } from "react";

const context = createContext<JWTContext | undefined>(undefined);
context.displayName = "JWTContext";

const { Provider } = context;

interface Props {
    token: string | undefined
}

interface JWTContext {
    raw: string
    parsed: {
        header: any
        content: any
    }
}

export function JWTProvider({ token, children }: PropsWithChildren<Props>) {
    if (!token) {
        return (
            <Provider value={ undefined }>
                { children }
            </Provider>
        );
    }

    const ctx: JWTContext = Object.defineProperties({
        raw: token,
    }, {
        parsed: {
            get() {
                const [ header, content ] = ctx.raw
                    .split(".")
                    .slice(0, 2)
                    .map(value => Buffer.from(value, "base64").toString())
                    .map(value => JSON.parse(value));

                return { header, content };
            },
        },
    });

    return (
        <Provider value={ ctx }>
            { children }
        </Provider>
    );
}

export function useJWT() {
    const value = useContext(context);

    return value?.raw;
}

export function useParsedJWT() {
    const value = useContext(context);

    return value?.parsed;
}
