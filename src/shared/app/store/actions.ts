import { ActionsOf, makeActionCreator } from "../../utils/actions";

export const applicationActions = {
    updateTitle: makeActionCreator("[APP] Update Title", (title: string) => ({ title })),
};

export type ApplicationActions = ActionsOf<typeof applicationActions>;
