import { applicationActions as actions, ApplicationActions } from "./actions";


export interface ApplicationState {
    title: string
}

const initialState: ApplicationState = {
    title: "Presenter",
};

export function applicationReducer(state: ApplicationState = initialState, action: ApplicationActions): ApplicationState {
    switch (action.type) {
        case actions.updateTitle.type:
            return {
                ...state,
                title: action.title
            };
    }

    return state;
}
