import { createContext, useContext, useContextSelector } from "use-context-selector";
import React, { PropsWithChildren } from "react";
import { useLocation } from "react-router-dom";

type Context = { [k: string]: string }

const context = createContext<Context>({});

export function useCookie<T>(selector: (cookies: Context) => T): T | undefined;
export function useCookie(): Context;
export function useCookie<T>(selector?: (cookies: Context) => T) {
    if (selector) {
        return useContextSelector(context, selector);
    } else {
        return useContext(context);
    }
}

function parseCookies() {
    return document.cookie
        .split(";")
        .map(value => value.split("="))
        .reduce(
            (map, [ key, value ]) => Object.assign(map, { [key]: value }),
            {},
        );
}


export function CookiesProvider({ children }: PropsWithChildren<{}>) {
    useLocation();

    const cookies = parseCookies();

    return (
        <context.Provider value={ cookies }>
            { children }
        </context.Provider>
    );
}
