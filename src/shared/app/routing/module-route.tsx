import React, { Fragment, useEffect } from "react";
import { Route } from "react-router-dom";
import { ModuleRoute } from "./models/route";
import { createRoutes } from "./index";
import { Module } from "./models/module";

interface Props extends ModuleRoute {
  prefix?: string
}

interface ContentProps {
  hook: Module["hook"]
}

function ModuleContent(props: React.PropsWithChildren<ContentProps>) {
  if (props.hook) {
    props.hook();
  }

  return (
      <Fragment>
        { props.children }
      </Fragment>
  );
}

export function ModuleRouteElement({ path, prefix, props, module }: Props) {
  return (
      <Route path={ props?.path || path } { ...props }>
        <ModuleContent hook={ module.hook }>
          { createRoutes(module.routes, prefix) }
        </ModuleContent>
      </Route>
  );
}
