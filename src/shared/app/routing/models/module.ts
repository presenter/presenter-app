import { ServerRoute } from "./route";
import { EffectCallback } from "react";

export interface Module {
  hook?: () => void
  routes: ServerRoute[]
}
