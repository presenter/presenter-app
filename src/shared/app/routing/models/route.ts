import { Module } from "./module";
import { CustomRouteProps } from "../component-route";

export interface BaseRoute {
  path: string
  exact?: boolean
  props?: Partial<CustomRouteProps>
}

export interface ComponentRoute extends BaseRoute {
  title: string
  description?: string,
  component: CustomRouteProps["component"]
}

export interface ModuleRoute extends BaseRoute {
  module: Module
  exact?: false
}

export type ServerRoute = ComponentRoute | ModuleRoute
