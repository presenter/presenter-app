export class Http {
  csrf?: string;

  constructor() {
    const meta = document.head.querySelector("meta[name=csrf]");
    this.csrf = meta?.getAttribute("content") || undefined;
  }

  post(url: string, body: string) {
    return new Promise<any>((resolve, reject) => {
      const xhr = new XMLHttpRequest();
      xhr.open("POST", url);
      xhr.setRequestHeader("Content-Type", "application/json");

      if (this.csrf) {
        xhr.setRequestHeader("X-XSRF-TOKEN", this.csrf);
      }

      xhr.responseType = "json";

      xhr.onload = function () {
        if (xhr.readyState === XMLHttpRequest.DONE) {
          resolve(xhr.response);
        }
      };

      xhr.onerror = reject;

      xhr.send(body);
    });
  }
}

export function useHTTP() {
  return new Http();
}
