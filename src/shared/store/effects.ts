import { AnyAction, Middleware } from "redux";
import { State } from "./reducer";
import { Actions } from "./actions";
import { Subject } from "rxjs";
import { filter } from "rxjs/operators";
import { ActionCreator } from "../utils/actions";

export function effectMiddleware(effect$: Subject<AnyAction>): Middleware<{}, State> {
    return store => next => (action: Actions) => {
        effect$.next(action);
        next(action);
    };
}

export function ofType<T extends ActionCreator>(actionCreator: T) {
    return filter((action: AnyAction): action is ReturnType<T> => action.type === actionCreator.type);
}
