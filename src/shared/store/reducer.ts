import { scrumReducer } from "../scrum/store/reducer";
import { combineReducers } from "redux";
import { applicationReducer } from "../app/store/reducer";

export const reducers = combineReducers({
    scrum: scrumReducer,
    application: applicationReducer
});

export type State = ReturnType<typeof reducers>
