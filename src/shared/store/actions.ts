import { scrumActions } from "../scrum/store/actions";
import { ActionsOf } from "../utils/actions";
import { applicationActions } from "../app/store/actions";


export const actions = {
    scrum: scrumActions,
    application: applicationActions
};

export type Actions = ActionsOf<typeof actions.scrum> | ActionsOf<typeof actions.application>;
