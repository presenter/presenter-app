import { AnyAction, applyMiddleware } from "redux";
import { composeWithDevTools } from "redux-devtools-extension/developmentOnly";
import { actions } from "./actions";
import { effectMiddleware } from "./effects";
import { Subject } from "rxjs";

function flatten<T>(obj: T): T[keyof T] {
    const out: any = {};

    for (const key in obj) {
        Object.assign(out, obj[key]);
    }

    return out;
}

const composeEnhancers = composeWithDevTools({
    actionCreators: flatten(actions),
});

export const effect$ = new Subject<AnyAction>();

export function enhancers() {
    return composeEnhancers(
        applyMiddleware(
            effectMiddleware(effect$),
        ),
    );
}
