import { Module } from "@routing/models/module";
import { ScrumBoard } from "./board/board";
import { ScrumProject } from "./project/project";
import { useScrumEffects } from "./store/effects";

export const scrumModule: Module = {
  routes: [
    {
      title: "Board",
      component: ScrumBoard,
      path: "/board",
      description: "Scrum Board",
      props: {
        path: "/board/:id",
      },
    },
    {
      title: "Project",
      component: ScrumProject,
      path: "/project",
      description: "Scrum Project",
      props: {
        path: "/project/:id",
      },
    },
  ],
  hook: () => {
    useScrumEffects();
  },
};
