import "./project.scss";

import { useHistory, useParams } from "react-router-dom";
import React, { useEffect, useMemo } from "react";
import { useScrumApi } from "../api";
import { useDispatch, useSelector } from "react-redux";
import { scrumActions } from "../store/actions";
import { scrumSelectors } from "../store/selector";
import { compose } from "redux";
import { Button } from "@material-ui/core";
import { makeUpdateSnapshotAction, useSnapshotID } from "../utils/snapshot";
import classNames from "classnames";
import FileCopy from "@material-ui/icons/FileCopy";
import Add from "@material-ui/icons/Add";

import cryptoRandomString from "crypto-random-string";


interface Params {
  id: string
}

export function ScrumProject() {
  const params = useParams<Params>();
  const projectID = params.id;

  const api = useScrumApi();
  const dispatch = useDispatch();
  const history = useHistory();

  const snapshotID = useSnapshotID();

  function loadProject() {
    api.fetchProjectByID(projectID)
        .then(project => {
          if (!project.snapshots) {
            throw new Error("no snapshots");
          }

          dispatch(scrumActions.updateProject({
            id: projectID,
            name: project.name,
            snapshots: project.snapshots,
            defaultSnapshot: project.default_snapshot,
          }));
        });
  }

  useEffect(loadProject, []);

  function setSnapshot(id: string) {
    return () => api.fetchSnapshot(id)
        .then(snapshot => dispatch(makeUpdateSnapshotAction(snapshot)));
  }

  function navigateToBoard(value: { id: string; name: string }): () => void {
    return () => history.push("/scrum/board/" + value.id + "?snapshot=" + snapshotID);
  }

  const snapshots = useSelector(compose(project => project.snapshots, scrumSelectors.project));
  const boards = useSelector(compose(snapshot => snapshot.boards, scrumSelectors.snapshot));

  const snapshotList = useMemo(() => (
      snapshots
          .sort((a, b) => Date.parse(a.timestamp) - Date.parse(b.timestamp))
          .map(value => (
              <Button key={ value.id } className={ classNames("snapshot-btn", value.id === snapshotID && "active") }
                      variant="outlined"
                      onClick={ setSnapshot(value.id) }>
                { value.name }
              </Button>
          ))
  ), [ snapshots, snapshotID ]);

  const boardList = useMemo(() => boards.map(value => (
      <Button key={ value.id } variant="outlined" onClick={ navigateToBoard(value) }
              className={ classNames("board-btn") }>
        { value.name }
      </Button>
  )), [ boards ]);

  function cloneSnapshot() {
    if (!snapshotID) {
      throw new Error("snapshot is undefined");
    }

    api.cloneSnapshot(snapshotID, cryptoRandomString({ length: 3 }))
        .then(() => loadProject());
  }

  function newBoard() {
    if (!snapshotID) {
      throw new Error("snapshot is undefined");
    }

    api.newBoard(snapshotID, cryptoRandomString({ length: 3 }))
        .then(() => loadProject());
  }

  return (
      <div>
        <div>
          <h2>Snapshots</h2>
          { snapshotList }
          <Button variant="outlined" onClick={ cloneSnapshot }>
            <FileCopy/>
          </Button>
        </div>
        <div>
          <h2>Boards</h2>
          { boardList }
          <Button className="project-btn" variant="outlined" onClick={ newBoard }>
            <Add/>
          </Button>
        </div>
      </div>
  );
}
