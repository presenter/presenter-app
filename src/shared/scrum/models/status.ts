export interface Status{
    id: string
    name: string
    stories: string[]
}
