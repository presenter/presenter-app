export interface Board {
  id: string
  name: string
  snapshot_id: string
}

export type BoardList = {
  id: string
  name: string
}[]
