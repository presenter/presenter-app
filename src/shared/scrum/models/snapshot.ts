import { BoardList } from "./board";

export interface Snapshot {
  id: string
  project_id: string
  timestamp: string
  name: string
  description?: string
  boards: BoardList
}

export type SnapshotList = {
  id: string
  name: string
  timestamp: string
  description?: string
}[]
