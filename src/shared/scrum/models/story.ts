export interface Story {
    title: string
    snapshotID: string
    statusID?: string
    effort?: number
    value?: number
}
