import React, { useCallback, useState } from "react";
import TextField from "@material-ui/core/TextField";
import { composition, useChainCallback } from "../../utils/chain";
import { pluckTargetValue } from "../../utils/events";


interface Props {
    onSubmit: (value: string) => any
}


export function StoryForm(props: Props) {
    const [ name, setName ] = useState("");

    return (
        <form onSubmit={ useChainCallback(
            [ name ],
            composition.method("preventDefault"),
            composition.constant(name),
            props.onSubmit,
        ) }>
            <TextField label="Name" onChange={ useChainCallback(pluckTargetValue, setName) } value={ name }
                       ref={ useCallback((x: HTMLDivElement) => x?.querySelector("input")?.focus(), []) }
            />
        </form>
    );
}
