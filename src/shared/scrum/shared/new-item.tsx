import "./new-item.scss";
import React from "react";
import Add from "@material-ui/icons/Add";
import ButtonBase, { ButtonBaseProps } from "@material-ui/core/ButtonBase";
import classNames from "classnames";


interface Props extends ButtonBaseProps {
    onClick: React.MouseEventHandler<HTMLButtonElement>
}

export function NewItem(props: Props) {
    const { onClick, className: extraClasses, ...forward } = props;

    return (
        <ButtonBase className={ classNames("new-item-container", extraClasses) } onClick={ props.onClick }
                    centerRipple={ true } { ...forward }
        >
            <Add/>
        </ButtonBase>
    );
}
