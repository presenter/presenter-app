import { DragDropContext, DropResult, ResponderProvided } from "react-beautiful-dnd";
import React, { useCallback } from "react";
import { useStore } from "react-redux";
import { State } from "../../store/reducer";
import { scrumActions, ScrumboardActions } from "../store/actions";
import { useScrumApi } from "../api";
import { ValueTypes } from "@graphql/graphql-zeus";
import lodash from "lodash";
import { useSnapshotID } from "../utils/snapshot";


interface Props {

}

export function ScrumBoardContext(props: React.PropsWithChildren<Props>) {
  const store = useStore<State>();
  const api = useScrumApi();

  const snapshot = useSnapshotID();

  function getColumn(id: string) {
    return store.getState().scrum.columns.values[id].stories;
  }

  const onDragEnd = useCallback(function (result: DropResult, provided: ResponderProvided) {
    if (!result.destination) {
      return;
    }

    const gql: ValueTypes["Mutation"][] = [];

    function updateStoryOrder(id: string, order: string[]) {
      gql.push({
        __alias: {
          [id.substring(0, 8)]: {
            updateStoryOrder: [ {
              snapshot,
              status: id,
              order: order,
            }, true ],
          },
        },
      });
      store.dispatch(scrumActions.updateStoryOrder({ id, order }));
    }

    const sourceID = result.source.droppableId;
    const destinationID = result.destination.droppableId;

    const source = [ ...getColumn(sourceID) ];
    source.splice(result.source.index, 1);

    const destinationIsSource = sourceID === destinationID;

    if (!destinationIsSource) {
      gql.push({
        updateStoryStatus: [ {
          snapshot,
          story: result.draggableId,
          status: destinationID,
        }, true ],
      });
      updateStoryOrder(sourceID, source);
    }

    let destination = destinationIsSource ? source : getColumn(destinationID);
    destination = [ ...destination.slice(0, result.destination.index), result.draggableId, ...destination.slice(result.destination.index) ];

    updateStoryOrder(destinationID, destination);

    return api.thunder.mutation(lodash.merge({}, ...gql));
  }, [ store, snapshot ]);

  return (
      <DragDropContext onDragEnd={ onDragEnd }>
        { props.children }
      </DragDropContext>
  );
}
