import "./board.scss";

import React, { useEffect } from "react";
import { ScrumBoardContext } from "./context";
import { NewItem } from "../shared/new-item";
import { StoryForm } from "../shared/story-form";
import { useModal } from "../../app/modal";
import { composition } from "../../utils/chain";
import { scrumActions } from "../store/actions";
import { useDispatchCallback } from "../../utils/redux";
import { useParams } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { State } from "../../store/reducer";
import { StoryColumn } from "./story-column";
import { useScrumApi } from "../api";
import { scrumSelectors } from "../store/selector";


interface Props {

}

export function ScrumBoard(props: Props) {
  const modal = useModal();
  const { id: boardID } = useParams<{ id: string }>();
  const api = useScrumApi();
  const dispatch = useDispatch();

  const project = useSelector(scrumSelectors.project);

  const columns = useSelector((state: State) => (
      state.scrum.columns.order.map(id => (
          <StoryColumn key={ id } id={ id }/>
      ))
  ));

  useEffect(function () {
    if (!project.id) {
      api.fetchProjectByBoardID(boardID)
          .then(project => {
            const snapshots = project.snapshots;

            if (!snapshots) {
              throw new Error("no snapshots");
            }

            dispatch(scrumActions.updateProject({
              id: project.id,
              name: project.name,
              snapshots,
              defaultSnapshot: project.default_snapshot,
            }));

            dispatch(scrumActions.updateBoard({ board: { id: boardID } }));
          });
    }
  }, [ project ]);

  useEffect(function () {
    dispatch(scrumActions.updateBoard({ board: { id: boardID } }));
  }, []);

  return (
      <ScrumBoardContext>
        <modal.component>
          <StoryForm onSubmit={ useDispatchCallback(
              (name: string) => scrumActions.newColumn({ name, boardID }),
              composition.tap(modal.close),
          ) }/>
        </modal.component>
        <div className="scrum-board-container">
          <div className="board">
            { columns }
            <NewItem onClick={ modal.open } aria-label="add column"/>
          </div>
        </div>
      </ScrumBoardContext>
  );
}
