import "./story-column.scss";
import React, { useCallback, useMemo } from "react";
import { Droppable } from "react-beautiful-dnd";
import { useDispatch, useSelector } from "react-redux";
import { State } from "../../store/reducer";
import { NewItem } from "../shared/new-item";
import { scrumSelectors } from "../store/selector";
import { scrumActions } from "../store/actions";
import { useScrumApi } from "../api";
import { Story } from "./story";
import { StoryForm } from "../shared/story-form";
import { useModal } from "../../app/modal";
import { useSnapshotID } from "../utils/snapshot";

interface Props {
  id: string
}

export function StoryColumn(props: Props) {
  const status = useSelector((state: State) => state.scrum.columns.values[props.id]);

  const snapshotID = useSnapshotID();
  const boardID = useSelector(scrumSelectors.boardID);

  const api = useScrumApi();
  const modal = useModal();

  const dispatch = useDispatch();

  const addStory = useCallback(function (title: string) {
    if (!snapshotID) {
      throw new Error("no snapshot selected");
    }

    api.newStory({
      title,
      snapshotID,
      statusID: props.id,
    }).then(function (story) {
      dispatch(scrumActions.newStory());
    });

    modal.close();
  }, [ snapshotID || boardID, modal ]);

  const storyMap = useSelector(scrumSelectors.stories);

  const stories = useSelector(scrumSelectors.columnStories(props.id))
      .map(id => storyMap[id]);

  const children = useMemo(() => (
          stories
              .map(value => value.id)
              .map((id, index) => <Story className="story" key={ id } id={ id } index={ index }/>)
      ), [ stories ],
  );


  return (
      <div className="story-column-container">
        <modal.component>
          <StoryForm onSubmit={ addStory }/>
        </modal.component>
        <div className="column-title">{ status.name }</div>
        <Droppable droppableId={ props.id }>
          { provided => (
              <div className="stories" ref={ provided.innerRef } { ...provided.droppableProps }>
                <div className="story-list">
                  { children }
                  { provided.placeholder }
                </div>
                <NewItem className="new-story-btn" onClick={ modal.open } aria-label="add story"/>
              </div>
          ) }
        </Droppable>
      </div>
  );
}
