import "./story.scss";
import React from "react";
import { useSelector } from "react-redux";
import { scrumSelectors } from "../store/selector";
import { Draggable } from "react-beautiful-dnd";
import classNames from "classnames";

interface Props extends React.DOMAttributes<HTMLDivElement> {
  id: string
  index: number
  className: string
}

export function Story(props: Props) {
  const story = useSelector(scrumSelectors.story(props.id));
  const { id, index, className: classes, ...forward } = props;

  return (
      <Draggable draggableId={ id } index={ index }>
        { provided => (
            <div className={ classNames("story-container", classes) } ref={ provided.innerRef }
                 { ...provided.draggableProps } { ...provided.dragHandleProps } { ...forward }>
              <div className="story-title">
                { story.title }
              </div>
            </div>
        ) }
      </Draggable>
  );
}
