import { scrumActions as actions, ScrumboardActions } from "./actions";
import { Status } from "../models/status";
import { Story } from "../models/story";
import { Snapshot, SnapshotList } from "../models/snapshot";
import { BoardList } from "../models/board";

export interface ScrumboardState {
  project: {
    id: string
    name: string
    snapshots: SnapshotList,
    defaultSnapshot: string
  },
  snapshot: Snapshot
  board: {
    id: string
  },
  columns: {
    values: { [k: string]: Status }
    order: string[]
  },
  stories: { [k: string]: Story & { id: string } }
}

const initialState: ScrumboardState = {
  project: { snapshots: [] } as any,
  board: {} as any,
  snapshot: { boards: [] as BoardList } as Snapshot,
  columns: {
    values: {},
    order: [],
  },
  stories: {},
};

export function scrumReducer(state: ScrumboardState = initialState, action: ScrumboardActions): ScrumboardState {
  switch (action.type) {
    case actions.updateBoard.type:
      return {
        ...state,
        board: action.board,
      };
    case actions.updateStories.type:
      return {
        ...state,
        stories: action.merge ? { ...state.stories, ...action.stories } : action.stories,
      };
    case actions.updateColumns.type:
      return {
        ...state,
        columns: {
          values: action.values || state.columns.values,
          order: action.order || state.columns.order,
        },
      };
    case actions.updateStoryOrder.type:
      return {
        ...state,
        columns: {
          ...state.columns,
          values: {
            ...state.columns.values,
            [action.id]: {
              ...state.columns.values[action.id],
              stories: action.order,
            },
          },
        },
      };
      // case actions.updateSnapshots.type:
      //   return {
      //     ...state,
      //     snapshots: action.snapshots
      //         .map(snapshot => ({ [snapshot.id]: snapshot }))
      //         .reduce((o, curr) => Object.assign(o, curr)),
      //   };
    case actions.updateSelectedSnapshot.type:
      return {
        ...state,
        snapshot: action.snapshot,
      };
    case actions.updateProject.type:
      return {
        ...initialState,
        project: {
          ...initialState.project,
          id: action.id,
          name: action.name,
          snapshots: action.snapshots,
          defaultSnapshot: action.defaultSnapshot,
        },
        snapshot: state.snapshot
      };
  }

  return state;
}
