import { State } from "../../store/reducer";
import { compose } from "redux";

const selectors = {
  state: (state: State) => state.scrum,
};

const a = Object.assign(selectors, {
  project: compose(state => state.project, selectors.state),
  snapshot: compose(state => state.snapshot, selectors.state),
  board: compose(state => state.board, selectors.state),
  stories: compose(state => state.stories, selectors.state),
  columns: compose(state => state.columns, selectors.state),
});

const b = Object.assign(a, {
  projectID: compose(state => state?.id, a.project),
  boardID: compose(state => state.id, a.board),
  story(id: string) {
    return compose(stories => stories[id], a.stories);
  },
  columnOrder: compose(state => state.order, a.columns),
  columnStories(id: string) {
    return compose(state => state.values[id].stories, a.columns);
  },
});

export { b as scrumSelectors };
