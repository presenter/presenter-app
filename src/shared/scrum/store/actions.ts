import { ActionsOf, makeActionCreator } from "../../utils/actions";
import { ScrumboardState } from "./reducer";
import { Story } from "../models/story";
import { Snapshot, SnapshotList } from "../models/snapshot";

export const scrumActions = {
  newProject: makeActionCreator("[SCRUM] New Project", (props: { name: string }) => props),
  newStory: makeActionCreator("[SCRUM] New Story"),
  newColumn: makeActionCreator("[SCRUM] New Column", (props: { name: string, boardID: string }) => props),
  newBoard: makeActionCreator("[SCRUM] New Board"),
  updateColumns: makeActionCreator("[SCRUM] Update Columns", (props: Partial<ScrumboardState["columns"]>) => props),
  updateProject: makeActionCreator("[SCRUM] Update Project", (props: { id: string, name: string, snapshots: SnapshotList, defaultSnapshot: string }) => props),
  updateStories: makeActionCreator("[SCRUM] Update Stories", (props: { stories: ScrumboardState["stories"], merge?: boolean }) => props),
  updateBoard: makeActionCreator("[SCRUM] Update Board", (props: { board: ScrumboardState["board"] }) => props),
  updateStoryOrder: makeActionCreator("[SCRUM] Update Story Order", (props: { id: string, order: string[] }) => props),
  updateSnapshots: makeActionCreator("[SCRUM] Update Snapshots", (props: { snapshots: Snapshot[] }) => props),
  updateSelectedSnapshot: makeActionCreator("[SCRUM] Update Selected Snapshot", (props: { snapshot: Snapshot }) => props),
};

export type ScrumboardActions = ActionsOf<typeof scrumActions>;
