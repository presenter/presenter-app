import { effect$ } from "../../store";
import { ofType } from "../../store/effects";
import { scrumActions } from "./actions";
import { useHistory } from "react-router-dom";
import { useLayoutEffect } from "react";
import { useThunder } from "../../thunder/thunder";
import { useDispatch, useSelector, useStore } from "react-redux";
import { useScrumApi } from "../api";
import { State } from "../../store/reducer";
import { distinctUntilChanged, filter, map, switchMap, take, tap, withLatestFrom } from "rxjs/operators";
import { BehaviorSubject, combineLatest } from "rxjs";
import { fromPromise } from "rxjs/internal-compatibility";
import { makeUpdateSnapshotAction } from "../utils/snapshot";


export function useScrumEffects() {
  function getQuerySnapshot() {
    return new URLSearchParams(location.search).get("snapshot");
  }


  const history = useHistory();
  const thunder = useThunder("presenter");
  const order = useSelector((state: State) => state.scrum.columns.order);
  const api = useScrumApi();
  const store = useStore();

  const state$ = new BehaviorSubject<State>(store.getState());

  store.subscribe(() => state$.next(store.getState()));

  const scrum$ = state$.pipe(
      map(value => value.scrum),
      distinctUntilChanged(),
  );

  const snapshot$ = scrum$.pipe(
      map(state => state.snapshot.id),
      filter(value => !!value),
      distinctUntilChanged(),
  );

  const dispatch = useDispatch();

  useLayoutEffect(function () {
    const subscriptions = [
      effect$.pipe(
          ofType(scrumActions.updateProject),
          withLatestFrom(scrum$.pipe(map(value => value.snapshot.id))),
          map(function ([ project, snapshot ]) {
            const querySnapshot = getQuerySnapshot();

            if (querySnapshot) {
              return querySnapshot;
            }

            if (snapshot && project.snapshots.some(value => value.id === snapshot)) {
              return snapshot;
            }

            return project.defaultSnapshot;
          }),
          switchMap(id => fromPromise(api.fetchSnapshot(id))),
          map(makeUpdateSnapshotAction),
          tap(dispatch),
      ).subscribe(),

      combineLatest([
        effect$.pipe(
            ofType(scrumActions.updateBoard),
            map(value => value.board.id),
        ),
        snapshot$,
      ]).pipe(
          tap(([ board, snapshot ]) => api.fetchStatuses(snapshot, board)),
      ).subscribe(),

      effect$.pipe(
          ofType(scrumActions.newColumn),
          withLatestFrom(snapshot$),
          tap(
              ([ action, snapshot ]) => thunder.mutation({
                    newStatus: [ {
                      name: action.name,
                      snapshot_id: snapshot,
                      board_id: action.boardID,
                    }, true ],
                  })
                  .then(() => api.fetchStatuses(snapshot, action.boardID)),
          ),
      ).subscribe(),

      effect$.pipe(
          ofType(scrumActions.newStory),
          switchMap(() => scrum$.pipe(take(1))),
          map(value => value.board.id),
          filter((value): value is string => !!value),
          withLatestFrom(snapshot$),
          tap(console.log),
          tap(([ boardID, snapshot ]) => api.fetchStatuses(snapshot, boardID)),
      ).subscribe(),
    ];

    return () => subscriptions.forEach(sub => sub.unsubscribe());
  }, []);
}
