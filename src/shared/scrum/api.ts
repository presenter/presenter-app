import { scrumActions } from "./store/actions";
import { useThunder } from "../thunder/thunder";
import { useDispatch } from "react-redux";
import { Story } from "./models/story";
import { UUID, ValueTypes } from "@graphql/graphql-zeus";
import { createMap } from "../utils/map";

function select<K extends keyof ValueTypes, T extends ValueTypes[K]>(type: K, fields: T) {
  return fields;
}

const d = select("SnapshotEntity", {
  id: true,
});


export function useScrumApi() {
  const thunder = useThunder("presenter");

  const dispatch = useDispatch();

  return {
    thunder,
    fetchSnapshot(id: string) {
      return thunder.query({
        snapshot: [
          { id },
          {
            id: true,
            name: true,
            boards: {
              id: true,
              name: true,
            },
            timestamp: true,
            description: true,
            project_id: true,
          },
        ],
      }).then(value => {
        if (!value.snapshot) {
          throw new Error("snapshot is undefined");
        }

        return value.snapshot;
      });
    },

    fetchSnapshotsByProjectID(projectID: string) {
      return thunder.query({
        snapshotsByProjectID: [
          { project_id: projectID },
          {
            id: true,
            timestamp: true,
            name: true,
            description: true,
          },
        ],
      });
    },

    fetchSnapshotsByBoardID(boardID: string) {
      return thunder.query({
            snapshotsByBoardID: [
              { board_id: boardID },
              {
                id: true,
                project_id: true,
                timestamp: true,
                name: true,
                description: true,
              },
            ],
          })
          .then(value => value.snapshotsByBoardID || []);
    },

    fetchProjectByID(id: string) {
      return thunder.query({
        project: [
          { id },
          {
            name: true,
            default_snapshot: true,
            snapshots: {
              id: true,
              timestamp: true,
              name: true,
              description: true,
            },
          },
        ],
      }).then(value => {
        if (!value.project) {
          throw new Error("field 'project' is missing");
        }
        return value.project;
      });
    },

    fetchProjectByBoardID(board: string) {
      return thunder.query({
            projectByBoardID: [
              { board },
              {
                id: true,
                default_snapshot: true,
                name: true,
                snapshots: {
                  id: true,
                  name: true,
                  project_id: true,
                  timestamp: true,
                  description: true,
                },
              },
            ],
          })
          .then(value => value.projectByBoardID);
    },

    fetchBoardsBySnapshotID(snapshot: string) {
      return thunder.query({
        snapshot: [
          { id: snapshot },
          {
            boards: {
              id: true,
              name: true,
            },
          },
        ],
      });
    },

    async fetchStatuses(snapshotID: string, boardID: string) {
      thunder.query({
        board: [
          { snapshot: snapshotID, id: boardID },
          {
            statuses: {
              id: true,
              name: true,
              stories: { id: true, title: true },
            },
          },
        ],
      }).then(data => {

        const statuses = data.board?.statuses;

        if (!statuses) {
          throw new Error("no statuses");
        }

        const map = statuses
            // @ts-ignore
            .map(status => ({ [status.id]: { ...status, stories: status.stories.map(value => value?.id) } }))
            .reduce((obj, curr) => Object.assign(obj, curr), {});

        const stories = statuses
            .map(status => status?.stories?.map(story => ({
              id: story?.id,
              title: story?.title,
              statusID: status.id,
              boardID,
            })))
            .reduce((map, stories) => Object.assign(map, createMap(stories || [], "id")), {});

        dispatch(scrumActions.updateStories({ stories }));

        dispatch(scrumActions.updateColumns({
          values: map,
          order: Object.keys(map),
        }));
      });
    },

    fetchStories(snapshotID: string) {
      const storyFields: ValueTypes["StoryEntity"] = { id: true, status_id: true, title: true };

      let stories: Promise<any[] | undefined>;

      stories = thunder.query({
        snapshot: [
          {
            id: snapshotID,
          },
          {
            stories: {
              id: true,
              title: true,
              description: true,
              status_id: true,
            },
          },
        ],
      }).then(data => data.snapshot?.stories);

      stories
          .then(value => (
              value
                  ?.map((value): Story & { id: string } => ({
                    snapshotID,
                    id: value.id,
                    title: value.title,
                    statusID: value.status_id,
                  }))
                  .reduce(
                      (map, value) => Object.assign(map, { [value.id]: value }),
                      {}) || {}
          ))
          .then(stories => scrumActions.updateStories({ stories }))
          .then(dispatch);
    },

    cloneSnapshot(snapshot: string, name: string) {
      return thunder.mutation({
        cloneSnapshot: [
          { source: snapshot, name },
          true,
        ],
      }).then(value => value.cloneSnapshot);
    },

    newProject(name: string): Promise<UUID> {
      return thunder.mutation({
            newProject: [ {
              name,
            }, true ],
          })
          .then(data => data.newProject);
    },

    newBoard(snapshot: string, name: string) {
      return thunder.mutation({
        newBoard: [
          { snapshot_id: snapshot, name },
          true,
        ],
      });
    },

    newStory(story: Story) {
      return thunder.mutation({
        newStory: [ {
          title: story.title,
          snapshot_id: story.snapshotID,
          status_id: story.statusID,
        }, true ],
      }).then(value => value.newStory);
    },

    updateStoryStatus(story: string, status: string) {
      return thunder.mutation({
        updateStoryStatus: [ {
          story,
          status,
        }, true ],
      });
    },

    // updateStoryOrder(snapshot: string, status: string, order: string[]) {
    //   return thunder.mutation({
    //     updateStoryOrder: [ {
    //       snapshot,
    //       status,
    //       order,
    //     }, true ],
    //   });
    // },
  };
}

export type ScrumAPI = ReturnType<typeof useScrumApi>;
