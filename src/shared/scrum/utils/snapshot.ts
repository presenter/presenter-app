import { useSelector } from "react-redux";
import { scrumSelectors } from "../store/selector";
import { compose } from "redux";
import { useLocation } from "react-router-dom";
import { SnapshotEntity, ValueTypes } from "@graphql/graphql-zeus";
import { scrumActions } from "../store/actions";
import { BoardList } from "../models/board";

export function useQuerySnapshotID() {
  return new URLSearchParams(useLocation().search).get("snapshot");
}

export function useSnapshotID() {
  return (
      useQuerySnapshotID()
      || useSelector(compose(snapshot => snapshot.id, scrumSelectors.snapshot))
      || null
  );
}

export function makeUpdateSnapshotAction(snapshot: {id: string, name: string, project_id: string, timestamp: string, description?: string, boards?: BoardList}) {
  return scrumActions.updateSelectedSnapshot({
    snapshot: {
      id: snapshot.id,
      name: snapshot.name,
      project_id: snapshot.project_id,
      timestamp: snapshot.timestamp,
      description: snapshot.description,
      boards: snapshot.boards || [],
    },
  });
}
